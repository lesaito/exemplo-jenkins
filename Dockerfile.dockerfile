FROM java:8
WORKDIR /
ADD Api-Investimentos-0.0.1-SNAPSHOT.jar Api-Investimentos-0.0.1-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "Api-Investimentos-0.0.1-SNAPSHOT.jar"]
CMD ["java -jar Api-Investimentos-0.0.1-SNAPSHOT.jar"]
