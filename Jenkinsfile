pipeline {
    agent any
    environment {
        IMAGE_DOCKER = "registry-itau.mastertech.com.br/api-investimento-leandro"
        REGISTRY_URL = "https://registry-itau.mastertech.com.br"

        def mysqlPort = sh (
            script: "awk -v min=64000 -v max=65000 'BEGIN{srand(); print int(min+rand()*(max-min+1))}'",
            returnStdout: true
        ).trim()

        MYSQL_PORT = "$mysqlPort"
    }
    post {
        failure {
            echo "======== Pipeline falhou! ========"
            updateGitlabCommitStatus name: "build", state: "failed"
        }
        success {
            echo "======== Pipeline executada com sucesso ========"
            updateGitlabCommitStatus name: "build", state: "success"
        }
        always {
            cleanWs()
        }
    }
    options {
        gitLabConnection("gitlab")
    }
    triggers {
        gitlab(triggerOnPush: true, triggerOnMergeRequest: true, branchFilterType: "All")
    }
    stages {
        stage('clean') { 
            steps { 
                sh "chmod a+x mvnw" 
                sh "./mvnw clean" 
            } 
        } 
        stage ('build') {
            steps {
                sh './mvnw clean compile'
            }
        }
        stage('test') {
            steps {
                script {
                    docker.image('mysql:5').withRun("-e 'MYSQL_ROOT_PASSWORD=admin' -e 'MYSQL_DATABASE=mastertech' -p $mysqlPort:3306") { c ->
                        /* Wait until mysql service is up */
                        sh "while ! mysqladmin ping -h 0.0.0.0 -P $mysqlPort --silent; do sleep 1; done"
                        /* Run some tests which require MySQL */
                        sh './mvnw test'
                    }   
                }
            }   
        }
        stage("package"){
            steps{
                sh './mvnw package'
            }        
        }
        stage("build_docker_image") {
            steps {
                script {     
                    app = docker.build("registry-itau.mastertech.com.br/api-investimento-leandro", "-f Dockerfile.dockerfile ./")
                    
                    withCredentials([
                        usernamePassword(
                            credentialsId: 'registry_credential',
                            passwordVariable: 'PASSWORD',
                            usernameVariable: 'USERNAME')
                    ]) {
                        sh "docker login -p $PASSWORD -u $USERNAME registry-itau.mastertech.com.br"
                    }
                    docker.withRegistry("https://registry-itau.mastertech.com.br", "registry_credential") {
                        app.push("${env.BUILD_NUMBER}")
                        app.push("latest")
                    }            
                }   
            }
        }

        stage("deploy"){
            steps{
                 sh "ssh -t ubuntu@18.223.97.175 '/home/ubuntu/deploy.sh api-investimento-jenkins api-investimento'"
            }        
        }
    }
}